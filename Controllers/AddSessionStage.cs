using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace baam.Pages
{
    [Route("[controller]")]
    public class AddSessionStage : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public AddSessionStage(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost("{code}")]
        public async Task<IActionResult> Index([FromRoute] string code)
        {
            // First of all we want to check if there is already a running session
            // If there is none - no problem, just start this one
            // If there is - we have two options:
            // 1) The session is "trash" (no attendance marks). It can be safely deleted
            // 2) The session has some marks. In this case we want to save the marks, so we mark it as finished
            // After this we start another stage of this session
            
            var user = HttpContext.User.GetUpn();

            var target = _dbContext.AttendanceSessions
                .SingleOrDefault(s => s.Id == Base36.Decode(code));

            if (target == null)
                return NotFound();

            if (target.Instructor != user)
                return Forbid();

            if (target.StagesCompleted >= 10)
                return BadRequest("Too much attendance session stages");
            
            var running = _dbContext.AttendanceSessions
                .Select(s => new {s, s.Attendance.Count})
                .SingleOrDefault(p => p.s.Running && p.s.Instructor == user);

            if (running != null)
            {
                if (running.Count == 0)
                    _dbContext.AttendanceSessions.Remove(running.s);
                else
                {
                    running.s.Running = false;
                    running.s.StagesCompleted++;
                }
            }

            target.Running = true;

            await _dbContext.SaveChangesAsync();

            return Redirect("/AttendanceCheck");
        }
    }
}
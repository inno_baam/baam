using Microsoft.AspNetCore.Mvc;

namespace baam.Pages
{
    [Route("[controller]")]
    public class SignOut : Controller
    {
        [HttpPost]
        public IActionResult Index()
        {
            return SignOut("OpenIdConnect", "Cookies");  
        }
    }
}
using System;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Text.Unicode;
using System.Threading.Tasks;
using baam.Config;
using baam.Model.Api;
using baam.Model.Db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ApiAttendanceSession = baam.Model.Api.AttendanceSession;
using DbAttendanceSession = baam.Model.Db.AttendanceSession;

namespace baam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttendanceSessionController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<AttendanceSessionController> _logger;
        private readonly AttendanceSessionConfiguration _config;

        public AttendanceSessionController(ApplicationDbContext context, ILogger<AttendanceSessionController> logger, 
            IOptions<AttendanceSessionConfiguration> config)
        {
            _dbContext = context;
            _logger = logger;
            _config = config.Value;
        }

        [HttpGet]
        public IActionResult GetSessions()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(_dbContext.AttendanceSessions.Where(s => !s.Running && s.Instructor == HttpContext.User.GetUpn())
                .AsEnumerable()
                .Select(s => 
                    new ListingAttendanceSession
                    {
                        Title = s.Title,
                        StartDate = s.StartDate,
                        SessionCode = s.SessionCode
                    }).ToArray());
        }
        
        [HttpGet("{code}")]
        public IActionResult GetSession([FromRoute] string code)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var session = _dbContext.AttendanceSessions
                .Where(_ => _.Id == Base36.Decode(code))
                .Include(_ => _.Attendance)
                .SingleOrDefault();
            
            if (session == null)
                return NotFound();

            if (session.Instructor != HttpContext.User.GetUpn())
                return Forbid();



            var attendance = session.Attendance
                .GroupBy(m => m.Student)
                .OrderBy(g => g.First().Time)
                .Select(g => (g.Key,
                        Enumerable.Range(0, session.StagesStarted)
                            .Select(s => g.Any(m => m.Stage == s))
                            .ToImmutableArray()
                    )
                ).ToImmutableArray();
            
            return Ok(new ApiAttendanceSession
            {
                Title = session.Title,
                SessionCode = session.SessionCode,
                AttendanceMarks = attendance,
                LastStageIndex = session.LastStageIndex,
            });
        }

        [HttpPost("getOrStart")]
        public async Task<IActionResult> GetOrStartSession()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = HttpContext.User.GetUpn();
            
            var res = _dbContext.AttendanceSessions
                .Select(s => new {s, s.Attendance.Count})
                .SingleOrDefault(t => t.s.Running && t.s.Instructor == user);

            var session = res?.s;

            if (res?.Count == 0)
            {
                _dbContext.Remove(res!.s);
                session = null;
            }
            
            if (session == null)
            {
                session = new DbAttendanceSession
                {
                    Instructor = user,
                    Running = true,
                    StagesCompleted = 0,
                    StartDate = DateTime.UtcNow,
                    AttendanceSeed = Utils.MakeSecureSeed(),
                    Title = "Untitled attendance session"
                };
                
                await _dbContext.AttendanceSessions.AddAsync(session);
                await _dbContext.SaveChangesAsync();
            }
            
            _logger.LogInformation($"Starting session {session.Id}");

            return Ok(new AttendanceSessionStartResult
            {
                Title = session.Title,
                AttendanceSeed = session.AttendanceSeed,
                SessionCode = session.SessionCode,
                InitialIteration = session.GetFirstNonStaleIteration(_config.SecretCodeIntervalMs!.Value),
                SecretCodeIntervalMs = _config.SecretCodeIntervalMs!.Value,
                CurrentStage = session.LastStageIndex,
            });
        }

        [HttpPost("{code}/stop")]
        public async Task<IActionResult> StopSession([FromRoute] string code)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = HttpContext.User.GetUpn();
            
            var session = _dbContext.AttendanceSessions.SingleOrDefault(s => s.Running && s.Instructor == user
                && s.Id == Base36.Decode(code));

            if (session == null)
                return NotFound();

            session.Running = false;
            session.StagesCompleted++; // fixme: Races possible. Nobody cares, but optimistic concurrency should be used

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        
        [HttpPut("{code}")]
        public async Task<IActionResult> UpdateSession([FromRoute] string code, [FromBody] AttendanceSessionUpdate update)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var session = await _dbContext.AttendanceSessions.FindAsync(checked((int)Base36.Decode(code)));
            
            if (session == null)
                return NotFound();

            if (session.Instructor != HttpContext.User.GetUpn())
                return Forbid();

            session.Title = update.Title;

            await _dbContext.SaveChangesAsync();

            return Ok();
        }
        

        private string Hash(string seed, int index)
        {
            var ms = new MemoryStream();
            ms.Write(Encoding.UTF8.GetBytes(seed));
            ms.Write(BitConverter.GetBytes(index));
            
            var hash = SHA1.HashData(ms.ToArray());
            return Convert.ToBase64String(hash[..6]).Replace('/', '_').Replace("=", "");
        }
        
        private bool ValidateChallenge(DbAttendanceSession session, string secretCode, DateTime submissionTime)
        {
            var hash = secretCode[..8];
            var index = checked((int)Base36.Decode(secretCode[8..]));

            var realHash = Hash(session.AttendanceSeed, index);

            if (hash != realHash)
                return false;
            
            var expectedRevealTime = session.StartDate + TimeSpan.FromMilliseconds(_config.SecretCodeIntervalMs!.Value) * index;
            var expectedRotateTime = expectedRevealTime + TimeSpan.FromMilliseconds(_config.SecretCodeIntervalMs.Value);
            TimeSpan difference;
            if (submissionTime < expectedRevealTime)
                difference = expectedRevealTime - submissionTime;
            else if (submissionTime > expectedRotateTime)
                difference = submissionTime - expectedRotateTime;
            else
                difference = TimeSpan.Zero;
            
            // difference shows by how much the submission time differs from either the reveal time or rotate time

            var res = difference < TimeSpan.FromMilliseconds(_config.ChallengeSubmissionTimeToleranceMs!.Value);

            _logger.LogDebug($"Difference is {difference}; {(res ? "pass" : "fail")}");

            
            return res;
        }

        [HttpPost("{code}/submitChallenge")]
        public async Task<IActionResult> SubmitChallenge([FromRoute] string code, [FromBody] string secretCode)
        {
            var submissionTime = DateTime.UtcNow;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            var user = HttpContext.User.GetUpn();
            var sessionId = checked((int) Base36.Decode(code));
            
            var session = await _dbContext.AttendanceSessions.FindAsync(sessionId);
            
            if (session == null)
            {
                _logger.LogDebug($"Session {code} does not exists");
                return new NotFoundResult();
            }

            if (!session.Running)
            {
                _logger.LogDebug($"Session {code} is not running");
                return new NotFoundResult();
            }

            if (!ValidateChallenge(session, secretCode, submissionTime))
            {
                _logger.LogDebug($"Challenge result {secretCode} validation failed");
                return new ForbidResult();
            }
            
            _logger.LogDebug($"Challenge result {secretCode} validation succeeded, giving an attendance mark");

            var mark = _dbContext.AttendanceMarks.SingleOrDefault(_ => _.SessionId == sessionId
                                                                       && _.Student == user
                                                                       && _.Stage == session.StagesCompleted);
            
            // silently ignore existing marks
            if (mark != null) 
                return Ok(code);
            
            await _dbContext.AttendanceMarks.AddAsync(new AttendanceMark
            {
                Session = session,
                Student = user,
                Manual = false,
                Stage = session.StagesCompleted,
            });

            await _dbContext.SaveChangesAsync();

            return Ok(code);
        }

        [HttpPost("{code}/addStudent")]
        public async Task<IActionResult> AddStudent([FromRoute] string code, [FromBody] string studentEmail)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = HttpContext.User.GetUpn();

            var session = _dbContext.AttendanceSessions.SingleOrDefault(m => 
                m.Instructor == user
                && m.Id == Base36.Decode(code));

            if (session == null)
                return NotFound();
            
            if (_dbContext.AttendanceMarks.Any(m => m.SessionId == session.Id &&
                                                    m.Student == studentEmail &&
                                                    m.Stage == session.LastStageIndex))
                return Ok();
            
            await _dbContext.AttendanceMarks.AddAsync(new AttendanceMark
            {
                Session = session,
                Student = studentEmail,
                Manual = true,
                Stage = session.LastStageIndex,
            });

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("{code}/removeStudent")]
        public async Task<IActionResult> RemoteStudent([FromRoute] string code, [FromBody] string studentEmail)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = HttpContext.User.GetUpn();

            var mark = _dbContext.AttendanceMarks.Where(m => 
                m.Session.Instructor == user
                && m.Session.Id == Base36.Decode(code)
                && m.Student == studentEmail).ToArray();

            if (mark.Length == 0)
                return NotFound();
            
            _dbContext.AttendanceMarks.RemoveRange(mark);

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("{code}/flipStudentState")]
        public async Task<IActionResult> FlipStudentState([FromRoute] string code, [FromBody] AttendanceMarkId id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = HttpContext.User.GetUpn();

            var session = _dbContext.AttendanceSessions.SingleOrDefault(m => 
                m.Instructor == user
                && m.Id == Base36.Decode(code));

            if (session == null)
                return NotFound();

            if (id.Stage > session.LastStageIndex)
                return BadRequest();
            
            var marks = _dbContext.AttendanceMarks.Where(m => 
                m.Session.Instructor == user
                && m.Session.Id == Base36.Decode(code)
                && m.Student == id.Email)
                .ToImmutableDictionary(m => m.Stage);

            if (marks.TryGetValue(id.Stage, out var mark))
                _dbContext.AttendanceMarks.Remove(mark);
            else
            {
                await _dbContext.AttendanceMarks.AddAsync(new AttendanceMark
                {
                    Session = session,
                    Student = id.Email,
                    Manual = true,
                    Stage = id.Stage,
                });
            }

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{code}")]
        public async Task<IActionResult> DeleteSession([FromRoute] string code)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = HttpContext.User.GetUpn();

            var session = await _dbContext.AttendanceSessions
                .FindAsync((int)Base36.Decode(code));

            if (session == null)
                return NotFound();
            
            if (session.Instructor != user)
                return NotFound();

            _dbContext.AttendanceSessions.Remove(session);

            await _dbContext.SaveChangesAsync();
            
            return Ok();
        }
    }
}
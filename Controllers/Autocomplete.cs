using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using baam.Model.Db;
using Microsoft.AspNetCore.Mvc;

namespace baam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Autocomplete : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;

        public Autocomplete(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public ImmutableArray<string> Index([FromBody] string query)
        {
            var user = HttpContext.User.GetUpn();

            // Well, this is dead simple "search" suggestions
            // Quite inefficient, does not scale way, but it works at our stage
            // It (tries to) rank suggestions based on number of times selected during the previous two months
            // Then it sorts alphabetically

            var now = DateTime.UtcNow;
            var cutoffDate = now - TimeSpan.FromDays(60);
            
            var res = _dbContext.AttendanceMarks
                .Where(m => m.Session.Instructor == user)
                .Select(m => m.Student)
                .Where(s => s.StartsWith(query))
                .Distinct()
                .OrderByDescending(s => _dbContext.AutocompleteSelectedSuggestions
                    .Count(ss =>
                        ss.User == user && ss.Suggestion == s && ss.SelectedDate > cutoffDate))
                .ThenBy(s => s)
                .Take(10)
                .ToImmutableArray();


            return res;
        }

        [HttpPost]
        [Route("onSelected")]
        public async Task OnSelected([FromBody] string suggestion)
        {
            var user = HttpContext.User.GetUpn();
            
            await _dbContext.AutocompleteSelectedSuggestions
                .AddAsync(new AutocompleteSelectedSuggestion
                {
                    SelectedDate = DateTime.UtcNow,
                    Suggestion = suggestion,
                    User = user
                });
            
            await _dbContext.SaveChangesAsync();
        }
    }
}
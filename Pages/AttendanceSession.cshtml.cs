using System.Linq;
using baam.Helpers;
using baam.Model.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using AttendanceSession = baam.Model.Db.AttendanceSession;

namespace baam.Pages
{
    public class AttendanceSessionModel : PageModel
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ExportHelper _exportHelper;

        public AttendanceSessionModel(ApplicationDbContext dbContext, ExportHelper exportHelper)
        {
            _dbContext = dbContext;
            _exportHelper = exportHelper;
        }

        public AttendanceSession Session { get; set; } = null!;
        
        [BindProperty] 
        public ExportParams ExportParams { get; set; } = null!;
        
        public IActionResult OnGet(string code)
        {
            var session = _dbContext.AttendanceSessions
                .SingleOrDefault(s => s.Id == Base36.Decode(code));

            if (session == null)
                return NotFound();

            var user = HttpContext.User.GetUpn();

            if (session.Instructor != user)
                return Forbid();

            Session = session;
            
            return Page();
        }
        
        public IActionResult OnPostExport()
        {
            if (!ModelState.IsValid)  // TODO: make sure SessionId=";;lfs" will not crash server
                return BadRequest();
            
            var user = HttpContext.User.GetUpn();

            return _exportHelper.HandleExport(ExportParams, user);
        }
    }
}
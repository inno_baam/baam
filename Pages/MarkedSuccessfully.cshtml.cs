using System;
using System.Collections.Immutable;
using System.Linq;
using baam.Model.Db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace baam.Pages
{
    public class MarkedSuccessfully : PageModel
    {
        private readonly ApplicationDbContext _db;

        public MarkedSuccessfully(ApplicationDbContext db)
        {
            _db = db;
        }
        
        public AttendanceSession Session { get; private set; } = null!;
        
        public ImmutableArray<(string email, ImmutableArray<bool> marks)> AttendanceMarks { get; set; }
        
        public IActionResult OnGet(string code)
        {
            var user = HttpContext.User.GetUpn();
            
            var marks = _db.AttendanceMarks.Where(m =>
                    m.SessionId == Base36.Decode(code)
                    && m.Stage ==
                    m.Session.StagesCompleted - (m.Session.Running ? 0 : 1))
                .OrderBy(m => m.Time)
                .Include(m => m.Session)
                .ToArray();

            var index = -1;
            
            for (var i = 0; i < marks.Length; i++)
                if (marks[i].Student == user)
                    index = i;

            if (index == -1)
                return NotFound();

            var take = Math.Min(index + 1, 6);
            var skip = index - take + 1;

            marks = marks.Skip(skip).Take(take).ToArray();

            Session = marks[0].Session;

            AttendanceMarks = marks
                .GroupBy(m => m.Student)
                .OrderBy(g => g.First().Time)
                .Select(g => (g.Key,
                        Enumerable.Range(0, Session.StagesStarted)
                            .Select(s => g.Any(m => m.Stage == s))
                            .ToImmutableArray()
                    )
                ).ToImmutableArray();

            return Page();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.IO;
using System.Linq;
using baam.Helpers;
using baam.Model.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AttendanceSession = baam.Model.Db.AttendanceSession;

namespace baam.Pages
{
    public class AttendanceSessions : PageModel
    {
        private readonly ApplicationDbContext _db;
        private readonly ExportHelper _exportHelper;

        public AttendanceSessions(ApplicationDbContext db, ExportHelper exportHelper)
        {
            _db = db;
            _exportHelper = exportHelper;
        }

        public ImmutableArray<(int id, string code, DateTime date, string title, int numberOfPeople, int numberOfStages)> Items { get; set; }

        public void OnGet()
        {
            var user = HttpContext.User.GetUpn();

            Items = _db.AttendanceSessions.Where(s => !s.Running && s.Instructor == user)
                .OrderByDescending(s => s.StartDate)
                .Select(s => new {s.Id, s.StartDate, s.Title, 
                    NumberOfPeople = s.Attendance.GroupBy(a => a.Student).Count(), s.StagesCompleted})
                .AsEnumerable()
                .Select(s => (s.Id, Base36.Encode(s.Id), s.StartDate, s.Title, s.NumberOfPeople, s.StagesCompleted))
                .ToImmutableArray();
        }

        [BindProperty] 
        public ExportParams ExportParams { get; set; } = null!;
        
        public IActionResult OnPostExport()
        {
            if (!ModelState.IsValid)  // TODO: make sure SessionId=";;lfs" will not crash server
                return BadRequest();
            
            var user = HttpContext.User.GetUpn();

            return _exportHelper.HandleExport(ExportParams, user);
        }
    }
}

﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace baam.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public string UserId { get; set; } = null!;
        
        public void OnGet()
        {
            UserId = HttpContext.User.GetUpn();
            
            _logger.LogInformation($"User: {UserId}");
        }
    }
}

using System.Linq;
using System.Threading.Tasks;
using baam.Model.Db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace baam.Pages
{
    public class AttendanceCheckModel : PageModel
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public AttendanceCheckModel(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
    }
}
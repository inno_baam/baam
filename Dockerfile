FROM adighe/wait AS wait
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env

WORKDIR /app

# workaround for https://github.com/NuGet/Home/issues/10491
RUN echo '<?xml version="1.0" encoding="utf-8"?>\
<configuration>\
  <packageSources>\
    <add key="nuget.org" value="https://api.nuget.org/v3/index.json" protocolVersion="3" />\
  </packageSources>\
</configuration>' >> NuGet.Config

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:8.0
WORKDIR /app

RUN usermod -u 1000 app && \
    echo '#!/bin/bash' > entrypoint \
    && echo 'rm -rf /tmp/tmp*' >> 'entrypoint' \
    && echo 'dotnet baam.dll' >> 'entrypoint' \
    && chmod +x entrypoint \
    && mkdir -p /app/data \
    && chown -R app:app /app/data 

VOLUME /app/data

USER app

ENTRYPOINT ["./entrypoint"]
    
COPY --from=wait /app/wait.sh /app/wait.sh
COPY --from=build-env /app/out .

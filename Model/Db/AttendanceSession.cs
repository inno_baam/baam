using System;
using System.Collections.Generic;

namespace baam.Model.Db
{
    public class AttendanceSession
    {
        public int Id { get; set; }
        
        public string Title { get; set; } = null!;
        public string Instructor { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public bool Running { get; set; }
        public string AttendanceSeed { get; set; } = null!;
        
        public int StagesCompleted { get; set; }
        public ICollection<AttendanceMark> Attendance { get; set; } = null!;
        
        public string SessionCode => Base36.Encode(Id);

        public int StagesStarted => StagesCompleted + (Running ? 1 : 0);
        public int LastStageIndex => StagesStarted - 1;

        public int GetFirstNonStaleIteration(int rotationIntervalMs)
        {
            var now = DateTime.UtcNow;
            var passedMs = (now - StartDate).TotalMilliseconds;
            return (int)Math.Ceiling(passedMs / rotationIntervalMs);
        }
    }
}
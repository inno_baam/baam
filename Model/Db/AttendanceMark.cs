using System;

namespace baam.Model.Db
{
    public class AttendanceMark
    {
        public int Id { get; set; }
        public string Student { get; set; } = null!;

        public DateTime Time { get; set; } = DateTime.UtcNow;
        public bool Manual { get; set; } = false;
        public int Stage { get; set; }
        
        public int SessionId { get; set; }
        public AttendanceSession Session { get; set; } = null!;
    }
}
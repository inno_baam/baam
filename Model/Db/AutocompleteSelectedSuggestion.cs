using System;

namespace baam.Model.Db
{
    public class AutocompleteSelectedSuggestion
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Suggestion { get; set; }
        public DateTime SelectedDate { get; set; }
    }
}
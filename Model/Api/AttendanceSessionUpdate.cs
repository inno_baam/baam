using System.ComponentModel.DataAnnotations;

namespace baam.Model.Api
{
    public class AttendanceSessionUpdate
    {
        [Required]
        public string Title { get; set; } = null!;
    }
}
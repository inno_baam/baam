using System;

namespace baam.Model.Api
{
    public class ListingAttendanceSession
    {
        public string SessionCode { get; set; } = null!;
        public string Title { get; set; } = null!;
        public DateTime StartDate { get; set; }
    }
}
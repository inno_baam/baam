using System;
using System.Collections.Immutable;

namespace baam.Model.Api
{
    public class AttendanceSession
    {
        public string SessionCode { get; set; } = null!;
        public string Title { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public int LastStageIndex { get; set; }
        public ImmutableArray<(string email, ImmutableArray<bool> marks)> AttendanceMarks { get; set; }
    }
}
namespace baam.Model.Api
{
    public class ExportParams
    {
        public enum ExportTypeEnum
        {
            Moodle,
            Txt,
            Raw,
        }
        public enum AttendanceConditionEnum
        {
            AtLeastOneStage,
            AllStages
        }
        
        public int SessionId { get; set; } = 0;
        public ExportTypeEnum ExportType { get; set; } = ExportTypeEnum.Moodle;
        public AttendanceConditionEnum AttendanceCondition { get; set; } = AttendanceConditionEnum.AtLeastOneStage;
    }
}
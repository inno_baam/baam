namespace baam.Model.Api
{
    public class AttendanceMarkId
    {
        public string Email { get; set; } = null!;
        public int Stage { get; set; }
    }
}
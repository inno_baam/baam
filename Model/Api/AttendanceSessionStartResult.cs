using System.Collections.Immutable;

namespace baam.Model.Api
{
    public class AttendanceSessionStartResult
    {
        public string Title { get; set; } = null!;
        public string SessionCode { get; set; } = null!;
        public string AttendanceSeed { get; set; } = null!;
        public int SecretCodeIntervalMs { get; set; }
        public int InitialIteration { get; set; }
        public int CurrentStage { get; set; }
    }
}
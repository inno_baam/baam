using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using baam.Config;
using baam.Helpers;
using baam.Model;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace baam
{
    public class Startup
    {
        private readonly IHostEnvironment _environment;

        public Startup(IConfiguration configuration, IHostEnvironment environment)
        {
            _environment = environment;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        private void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            if (options.SameSite == SameSiteMode.None)
            {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                if (Utils.DisallowsSameSiteNone(userAgent))
                {
                    options.SameSite = SameSiteMode.Unspecified;
                }
            }
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            using var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.SetMinimumLevel(LogLevel.Information);
                builder.AddConsole();
                builder.AddEventSourceLogger();
            });
            var logger = loggerFactory.CreateLogger("Startup");
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.Unspecified;
                options.OnAppendCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
            });
            
            services.AddAuthorization(options =>
            {
                options.FallbackPolicy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
            });

            services.AddDataProtection()
                .PersistKeysToDbContext<ApplicationDbContext>();
            
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
                })
                .AddCookie(o =>
                {
                    o.ExpireTimeSpan = TimeSpan.FromDays(365);
                    o.SlidingExpiration = true;
                })
                .AddOpenIdConnect(options =>
                {
                    var authConfig = new AuthConfiguration();
                    Configuration.GetSection("Auth").Bind(authConfig);
                    
                    options.SignInScheme = "Cookies";
                    options.Authority = Configuration.GetValue<string>("PublicUrl") ?? throw new NullReferenceException();
                    options.RequireHttpsMetadata = true;
                    options.ResponseType = OpenIdConnectResponseType.IdToken;
                    options.UsePkce = false;
                    options.Scope.Clear();
                    options.Scope.Add("openid email");
                    options.SaveTokens = false;
                    //options.UseTokenLifetime = true;

                    options.MetadataAddress = authConfig.MetadataAddress ?? throw new NullReferenceException();

                    options.ClientId = authConfig.ClientId ?? throw new NullReferenceException();
                    options.ClientSecret = authConfig.ClientSecret ?? throw new NullReferenceException();

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                         NameClaimType = "upn"
                    };
                    options.SecurityTokenValidator = new JwtSecurityTokenHandler
                    {
                        MapInboundClaims = false
                    };
                    
                    options.ClaimActions.Clear();
                    options.ClaimActions.Add(new MapUpnOnlyAction("upn"));

                    options.Events.OnTicketReceived = ctx =>
                    {
                        ctx.Properties.IsPersistent = true;
                        ctx.Properties.AllowRefresh = true;
                        return Task.CompletedTask;
                    };
                    
                    options.Events.OnRemoteFailure = r =>
                    {
                        logger.LogDebug("OnRemoteFailure");
                        var maxFailures = 6;
                        var failuresWithSeconds = 10;
                        if (r.Failure != null && r.Failure.Message == "Correlation failed.")
                        {
                            var rapidFailures = 0;
                            DateTimeOffset since = DateTime.UtcNow;
                            if (r.Request.Cookies.TryGetValue(".correlation.failures", out var correlationFailures))
                            {
                                var pipeIndex = correlationFailures!.IndexOf('|');
                                if (pipeIndex > 0)
                                {
                                    if (int.TryParse(correlationFailures.Substring(0, pipeIndex), out rapidFailures) &&
                                        long.TryParse(correlationFailures.Substring(pipeIndex + 1), out var secs))
                                    {
                                        since = DateTimeOffset.FromUnixTimeSeconds(secs);
                                    }
                                }
                            }

                            rapidFailures++;
                            if (rapidFailures < maxFailures)
                            {
                                r.HandleResponse();
                                r.Response.Cookies.Append(".correlation.failures",
                                    $"{rapidFailures}|{since.ToUnixTimeSeconds()}",
                                    new CookieOptions()
                                    {
                                        Expires = since + TimeSpan.FromSeconds(failuresWithSeconds), HttpOnly = true,
                                        Secure = true
                                    });
                                r.Response.Redirect(r.Properties!.RedirectUri!, false);
                            }
                        }

                        return Task.CompletedTask;
                    };

                });

            services
                .AddRazorPages()
                .AddRazorPagesOptions(o =>
                {
                    o.Conventions.AddPageRoute("/ScanQrCode", "/s");
                });

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                var cstr = Configuration.GetConnectionString("DefaultConnection");

                if (!_environment.IsProduction())
                {
                    options.EnableSensitiveDataLogging();
                    logger.LogDebug("Using sqlite connection string {0}", cstr);
                }

                options.UseSqlite(cstr);
            });
            
            services.AddOptions<AttendanceSessionConfiguration>()
                .Bind(Configuration.GetSection("AttendanceSession"))
                .ValidateDataAnnotations();

            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = 
                    Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            // Add helper services
            services.AddTransient<ExportHelper>();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

            services.AddHttpClient<ChangelogHelper>(client =>
            {
                client.BaseAddress =
                    new Uri(Configuration.GetValue<string>("BlogUrl")
                            ?? throw new NullReferenceException());
                client.Timeout = TimeSpan.FromSeconds(1);
            });
            services.AddSingleton<AndroidAppHelper>(provider =>
                new AndroidAppHelper(provider.GetRequiredService<ILogger<ChangelogHelper>>(), provider.GetRequiredService<HttpClient>(),
                    Configuration.GetValue<string>("AndroidRepoName") ?? throw new NullReferenceException()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext dataContext,
            ILogger<Startup> logger)
        {
            logger.LogInformation("Running in {0}, revision {1}", env.EnvironmentName, Utils.GitHash);

            if (env.IsDevelopment() || env.IsStaging())
            {
                app.UseDeveloperExceptionPage();
            }

            // we are OK with migrations on prod because there are backups
            dataContext.Database.Migrate();

            if (env.IsProduction() || env.IsStaging())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                                                                      | ForwardedHeaders.XForwardedHost,
                    KnownNetworks = { new Microsoft.AspNetCore.HttpOverrides.IPNetwork(IPAddress.Any, 0) } // breaks in docker
                });
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting(); 
            app.UseCookiePolicy();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }

    public class MapUpnOnlyAction : ClaimAction
    {
        public MapUpnOnlyAction(string claimType) : base(claimType, "http://www.w3.org/2001/XMLSchema#string")
        {
        }

        public override void Run(JsonElement userData, ClaimsIdentity identity, string issuer)
        {
            var upn = (identity.FindFirst("upn") ?? identity.FindFirst("email") ??
                identity.FindFirst("sub") ?? throw new KeyNotFoundException("Can't find upn claim")).Value;

            foreach (var claim in identity.Claims.ToArray())
                identity.RemoveClaim(claim);
            
            if (!upn.EndsWith("@innopolis.university") && !upn.EndsWith("@innopolis.ru"))
                throw new InvalidDataException("Expected the upn to end with either @innopolis.university or @innopolis.ru");
            
            identity.AddClaim(new Claim(ClaimType, upn));
        }
    }
}

using System.ComponentModel.DataAnnotations;

namespace baam.Model
{
    public class AuthConfiguration
    {
        public string ClientId { get; set; } = null!;
        public string ClientSecret { get; set; } = null!;
        public string MetadataAddress { get; set; } = null!;
    }
}
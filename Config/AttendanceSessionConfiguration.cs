using System.ComponentModel.DataAnnotations;

namespace baam.Config
{
    public class AttendanceSessionConfiguration
    {
        [Required]
        public int? CodeLength { get; set; }

        [Required]
        public int? SecretCodeCount { get; set; }
        
        [Required]
        public int? SecretCodeIntervalMs { get; set; }
        
        [Required]
        public int? ChallengeSubmissionTimeToleranceMs { get; set; }
    }
}
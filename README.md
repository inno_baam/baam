## Application startup


#### dotnet core SDK

Go to [microsoft site](https://dotnet.microsoft.com/download) and install .net CORE (not .net Framework) SDK

Also install aspnet runtime from there

#### Client secret
To start an application you have to provide a client secret for university login to work. It's secret, so, obviously, it's not included into the repository.

TL;DR: ask [@unb0und](https://t.me/unb0und) for a client secret, then, inside the app directory, run:

```
dotnet user-secrets set "Auth:ClientSecret" "INSERT SECRET HERE"
```

#### Start from CLI

Now you are ready to start the application. Running this in the app directory is good to go:

```
dotnet run
```

Open the browser and trust the development https certificate (the easiest way is just to ignore the error =))

#### Setting up IDE

Usually you don't develop the application w/o the IDE. [JetBrains Rider](https://www.jetbrains.com/rider/) is recommended (license is free for students). Using it with our project is as simple as opening the project in it =).

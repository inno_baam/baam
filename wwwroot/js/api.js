
baam = {};

baam.AttendanceSession = {};

baam.AttendanceSession.get = async function(code) {
    let res = await fetch("/api/AttendanceSession/" + code);
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return await res.json();
}

baam.AttendanceSession.update = async function(code, content) {
    let res = await fetch("/api/AttendanceSession/" + code, {method: 'PUT', body: JSON.stringify(content),
        headers: { "Content-Type": "application/json" }});
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return true;
}

baam.AttendanceSession.start = async function() {
    let res = await fetch("/api/AttendanceSession/getOrStart", {method: 'POST'});
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return await res.json();
}

baam.AttendanceSession.stop = async function(code) {
    let res = await fetch("/api/AttendanceSession/" + code + "/stop", {method: 'POST'});
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return true;
}

baam.AttendanceSession.submitChallenge = async function(code, secret) {
    let res = await fetch("/api/AttendanceSession/" + code + "/submitChallenge", {method: 'POST', body: JSON.stringify(secret), 
        headers: { "Content-Type": "application/json" }});
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return await res.text();
}

baam.AttendanceSession.addStudent = async function(code, email) {
    let res = await fetch("/api/AttendanceSession/" + code + "/addStudent", {method: 'POST', body: JSON.stringify(email),
        headers: { "Content-Type": "application/json" }});
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return true;
}

baam.AttendanceSession.removeStudent = async function(code, email) {
    let res = await fetch("/api/AttendanceSession/" + code + "/removeStudent", {method: 'POST', body: JSON.stringify(email),
        headers: { "Content-Type": "application/json" }});
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return true;
}

baam.AttendanceSession.remove = async function(code) {
    let res = await fetch("/api/AttendanceSession/" + code, {method: 'DELETE'});
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return true;
}

baam.AttendanceSession.flipStudentState = async function(code, email, stage) {
    let res = await fetch("/api/AttendanceSession/" + code + "/flipStudentState", {method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ email, stage })
    });
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return true;
}

baam.Autocomplete = async function(query) {
    let res = await fetch("/api/Autocomplete", {method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(query)
    });
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return res.json();
}

baam.Autocomplete.onSelected = async function(suggestion) {
    let res = await fetch("/api/Autocomplete/onSelected", {method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(suggestion)
    });
    if (!res.ok)
        throw new Error('Got HTTP response: ' + res.status);
    return true;
}
﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function arrayEquals(a, b) {
    return Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index]);
}

function get_or_default(obj, key, def) {
    const res = obj[key];
    if (res === undefined)
        return def;
    return res;
}

class StudentListRenderer {
    
    constructor(session_code, el, update_handler, opts) {
        this.state = new Map();
        
        this.session_code = session_code;
        this.el = el;
        this.update_handler = update_handler;
        this.readonly = get_or_default(opts, 'readonly', false);
        this.highlight_self = get_or_default(opts, 'highlight_self', false);
        this.show_stages = get_or_default(opts, 'show_stages', true);
    }
    
    render_student_entry_into(target, student_email, marks) {
        const float = $("<span class='ml-auto d-flex'></span>");
        target.append(student_email).append(float);

        // globals are bad
        // JS modules will (probably) solve this...
        // But an actual front framework is (usually) required for this =)
        if (this.highlight_self && student_email === username) {
            target.addClass("student-list-element-self");
        }
        
        if (this.show_stages && marks.length > 1) {
            const butt_group = $("<div class='btn-group btn-group-sm mr-3' role='group'></div>");

            let i = 0;
            for (const mark of marks) {
                const index = i++;
                const butt = $("<button type='button' class='btn'></button>").append(index+1);
                butt.addClass("btn-" + (mark ? "success" : "outline-warning"))
                if (!this.readonly) {
                    butt.click(async () => {
                        try {
                            await baam.AttendanceSession.flipStudentState(this.session_code, student_email, index);
                        } catch (t) {
                            $.snack('error', t, 2000);
                        }

                        this.update_handler();
                    })
                }

                butt_group.append(butt);
            }

            float.append(butt_group);
        }

        if (!this.readonly) {
            const butt = $("<button type='button' class='close'>×</button>");
            butt.click(async () => {
                try {
                    await baam.AttendanceSession.removeStudent(this.session_code, student_email);
                    $.snack('warning', 'Student removed', 1000);
                } catch (t) {
                    $.snack('error', t, 2000);
                }
                this.update_handler();
            })

            float.append(butt);
        }
        return target;
    }
    
    add_entry(email, marks) {
        const entry_el = $("<li></li>");
        this.render_student_entry_into(entry_el, email, marks);
        this.el.append(entry_el);

        const entry = {'marks': marks, 'el': entry_el };
        
        this.state.set(email, entry);
        
        return entry;
    }
    
    rerender_entry_incremental(email, entry, new_marks) {
        if (!arrayEquals(entry.marks, new_marks)) {
            entry.el.empty();
            this.render_student_entry_into(entry.el, email, new_marks)
            entry.marks = new_marks;
        }
    }
    
    remove_entry(email, entry) {
        entry.el.remove();
        this.state.delete(email);
    }
    
    rerender_incremental(list) {
        const leftover_keys = new Set(this.state.keys());
        for (let s of list) {
            leftover_keys.delete(s.item1);
            const entry = this.state.get(s.item1);
            if (entry !== undefined) {
                this.rerender_entry_incremental(s.item1, entry, s.item2);
            } else {
                this.add_entry(s.item1, s.item2);
            }
        }
        for (let email of leftover_keys) {
            this.remove_entry(email, this.state.get(email));
        }
    }
    
    update(list) {
        const old_top = this.el.scrollTop()
        const at_bottom = old_top === (this.el[0].scrollHeight - this.el.outerHeight())

        this.rerender_incremental(list);
        
        // https://stackoverflow.com/questions/3898130/check-if-a-user-has-scrolled-to-the-bottom
        this.el.scrollTop(at_bottom ? this.el[0].scrollHeight : old_top);
    }
    
    empty() {
        return this.state.size === 0;
    }
}

function b64encode(buf) {
    let res = '';
    for (let i = 0; i < buf.length; i++)
        res += String.fromCharCode(buf[i]);
    return btoa(res);
}

function b64URLencode(buf) {
    return b64encode(buf).replace(/\//g, '_').replace(/=/g, '');
}

function make_input_selectall(input) {
    let focusedElement;
    input.addEventListener('focus', function () {
        if (focusedElement === this) return; //already focused, return so user can now place cursor at specific point in input.
        focusedElement = this;
        focusedElement.select(); //select all text in any field on focus for easy re-entry.
    });
    input.addEventListener('blur', function() {
        focusedElement = null;
    })
}
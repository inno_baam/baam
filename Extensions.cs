using System;
using System.Security.Claims;

namespace baam
{
    public static class Extensions
    {
        public static string GetUpn(this ClaimsPrincipal user)
        {
            // TODO: do we still want to have the GetUpn method?
            return user.Identity?.Name ?? throw new InvalidOperationException("Attempt to get a upn from a user with no identity");
        }
    }
}
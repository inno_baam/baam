﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace baam.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AttendanceSessions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(nullable: false),
                    Instructor = table.Column<string>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Running = table.Column<bool>(nullable: false),
                    AttendanceSeed = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceSessions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AttendanceMarks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Student = table.Column<string>(nullable: false),
                    SessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceMarks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AttendanceMarks_AttendanceSessions_SessionId",
                        column: x => x.SessionId,
                        principalTable: "AttendanceSessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AttendanceMarks_SessionId",
                table: "AttendanceMarks",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_AttendanceMarks_Student",
                table: "AttendanceMarks",
                column: "Student");

            migrationBuilder.CreateIndex(
                name: "IX_AttendanceMarks_Student_SessionId",
                table: "AttendanceMarks",
                columns: new[] { "Student", "SessionId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttendanceMarks");

            migrationBuilder.DropTable(
                name: "AttendanceSessions");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace baam.Migrations
{
    public partial class UpdateAttendanceMarkPK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AttendanceMarks_Student_SessionId",
                table: "AttendanceMarks");

            migrationBuilder.CreateIndex(
                name: "IX_AttendanceMarks_Student_SessionId_Stage",
                table: "AttendanceMarks",
                columns: new[] { "Student", "SessionId", "Stage" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AttendanceMarks_Student_SessionId_Stage",
                table: "AttendanceMarks");

            migrationBuilder.CreateIndex(
                name: "IX_AttendanceMarks_Student_SessionId",
                table: "AttendanceMarks",
                columns: new[] { "Student", "SessionId" },
                unique: true);
        }
    }
}

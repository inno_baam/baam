﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace baam.Migrations
{
    public partial class Autocomplete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AutocompleteSelectedSuggestions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    User = table.Column<string>(nullable: false),
                    Suggestion = table.Column<string>(nullable: false),
                    SelectedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AutocompleteSelectedSuggestions", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AutocompleteSelectedSuggestions_SelectedDate",
                table: "AutocompleteSelectedSuggestions",
                column: "SelectedDate");

            migrationBuilder.CreateIndex(
                name: "IX_AutocompleteSelectedSuggestions_Suggestion_User",
                table: "AutocompleteSelectedSuggestions",
                columns: new[] { "Suggestion", "User" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AutocompleteSelectedSuggestions");
        }
    }
}

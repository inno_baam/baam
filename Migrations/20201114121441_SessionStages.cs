﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace baam.Migrations
{
    public partial class SessionStages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StagesCompleted",
                table: "AttendanceSessions",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "Stage",
                table: "AttendanceMarks",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StagesCompleted",
                table: "AttendanceSessions");

            migrationBuilder.DropColumn(
                name: "Stage",
                table: "AttendanceMarks");
        }
    }
}

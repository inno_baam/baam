﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace baam.Migrations
{
    public partial class AttendanceMarkMeta : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Manual",
                table: "AttendanceMarks",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Time",
                table: "AttendanceMarks",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Manual",
                table: "AttendanceMarks");

            migrationBuilder.DropColumn(
                name: "Time",
                table: "AttendanceMarks");
        }
    }
}

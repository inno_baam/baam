using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace baam.Helpers
{
    public class ChangelogHelper
    {
        private readonly ILogger<ChangelogHelper> _logger;
        private readonly HttpClient _httpClient;

        public ChangelogHelper(ILogger<ChangelogHelper> logger, HttpClient httpClient)
        {
            _logger = logger;
            _httpClient = httpClient;
        }

        public async Task<string> GetChangelogHtml()
        {
            try
            {
                var res = await _httpClient.GetAsync("latest.html");
                res.EnsureSuccessStatusCode();
                return await res.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while fetching the changelog");
                return "";
            }
        }
    }
}
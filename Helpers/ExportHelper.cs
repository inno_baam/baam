using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using baam.Model.Api;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AttendanceSession = baam.Model.Db.AttendanceSession;

namespace baam.Helpers
{
    public class ExportHelper
    {
        private readonly ApplicationDbContext _db;


        public ExportHelper(ApplicationDbContext db)
        {
            _db = db;
        }

        private IEnumerable<string> ProcessMultistageMarks(ExportParams @params, AttendanceSession session)
        {
            var marksCollect = session.Attendance
                .GroupBy(a => a.Student);

            var stages = Enumerable.Range(0, session.StagesCompleted);

            if (@params.AttendanceCondition == ExportParams.AttendanceConditionEnum.AllStages)
                marksCollect = marksCollect.Where(m =>
                    stages.All(s =>
                        m.Any(sm => sm.Stage == s)
                    )
                );
            else
                marksCollect = marksCollect.Where(m =>
                    stages.Any(s =>
                        m.Any(sm => sm.Stage == s)
                    )
                );

            return marksCollect.Select(m => m.Key);
        }
        
        private byte[] ExportMoodle(ExportParams @params, AttendanceSession session)
        {
            var csvData = new List<(string, string)>
            {
                ("External user field", "status")
            };
                
            
            foreach (var mark in ProcessMultistageMarks(@params, session)) 
                csvData.Add((mark, "P"));

            var ms = new MemoryStream();
            var sw = new StreamWriter(ms);

            foreach (var line in csvData)
                sw.WriteLine(line.Item1 + "," + line.Item2);
            sw.Flush();

            return ms.ToArray();
        }

        private byte[] ExportTxt(ExportParams @params, AttendanceSession session)
        {
            var ms = new MemoryStream();
            var sw = new StreamWriter(ms);

            foreach (var mark in ProcessMultistageMarks(@params, session))
                sw.WriteLine(mark);
            sw.Flush();

            return ms.ToArray();
        }

        private byte[] ExportRaw(ExportParams @params, AttendanceSession session)
        {
            var csvData = new List<(string, string, string, string)>
            {
                ("email", "stage", "time", "manual")
            };
            csvData.AddRange(
                session.Attendance.Select(mark => 
                    (mark.Student, 
                        mark.Stage.ToString(), 
                        mark.Time.ToString(CultureInfo.InvariantCulture), 
                        mark.Manual ? "1" : "0")
                )
            );

            var ms = new MemoryStream();
            var sw = new StreamWriter(ms);

            foreach (var line in csvData)
                sw.WriteLine(line.Item1 + "," + line.Item2 + "," + line.Item3 + "," + line.Item4);
            sw.Flush();

            return ms.ToArray();
        }
        
        public IActionResult HandleExport(ExportParams @params, string user)
        {
                
            var session = _db.AttendanceSessions
                .Where(s => s.Id == @params.SessionId && s.Running == false)
                .Include(s => s.Attendance)
                .SingleOrDefault();

            if (session == null)
                return new NotFoundResult();

            if (session.Instructor != user)
                return new ForbidResult();

            byte[] export = @params.ExportType switch
                {
                    ExportParams.ExportTypeEnum.Moodle => ExportMoodle(@params, session),
                    ExportParams.ExportTypeEnum.Txt => ExportTxt(@params, session),
                    ExportParams.ExportTypeEnum.Raw => ExportRaw(@params, session),
                    _ => throw new ArgumentOutOfRangeException()
                };

            string contentType = @params.ExportType switch
            {
                ExportParams.ExportTypeEnum.Txt => "text/plain",
                ExportParams.ExportTypeEnum.Moodle => "text/csv",
                ExportParams.ExportTypeEnum.Raw => "text/csv",
                _ => throw new ArgumentOutOfRangeException()
            };

            string suffix = @params.ExportType switch
            {
                ExportParams.ExportTypeEnum.Txt => ".txt",
                ExportParams.ExportTypeEnum.Moodle => ".csv",
                ExportParams.ExportTypeEnum.Raw => ".csv",
                _ => throw new ArgumentOutOfRangeException()
            };
            
            return new FileContentResult(export, contentType)
            {
                FileDownloadName = $"{session.Title}-attendance" + suffix
            };
        }
    }
}
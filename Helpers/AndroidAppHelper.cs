using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace baam.Helpers;

public class AndroidAppHelper
{
    private readonly ILogger<ChangelogHelper> _logger;
    private readonly HttpClient _httpClient;
    private readonly String _repoName;

    public AndroidAppHelper(ILogger<ChangelogHelper> logger, HttpClient httpClient, String repoName)
    {
        httpClient.BaseAddress = new Uri("https://api.github.com");
        httpClient.Timeout = TimeSpan.FromSeconds(1);
        httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("baam website (maintained by @DCNick3)");
        
        _logger = logger;
        _httpClient = httpClient;
        _repoName = repoName;
    }

    public String RepoName => _repoName;

    public async Task<string> GetLatestReleaseUrl()
    {
        try
        {
            var res = await _httpClient.GetAsync($"https://api.github.com/repos/{_repoName}/releases?page=1&per_page=10");
            res.EnsureSuccessStatusCode();
            var releases = await res.Content.ReadFromJsonAsync<List<GithubRelease>>(new JsonSerializerOptions());

            // _logger.LogDebug(JsonConvert.SerializeObject(releases));
            
            var orderedReleases = releases!.SelectMany(r =>
            {
                var version = new VersionId(r.Name);
                var asset = r.Assets
                    .FirstOrDefault(a => a.ContentType == "application/vnd.android.package-archive");
                GithubReleaseAsset[] assets = asset == null ? [] : [asset];

                return assets.Select(a => new { name = r.Name, version, url = a.BrowserDownloadUrl });
            })
                .OrderBy(v => v.version)
                .Where(v => v.version.IsStable())
            .ToList();

            var latestRelease = orderedReleases.Last();

            return latestRelease.url;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while fetching the latest app release");
            return $"https://github.com/{_repoName}/releases/latest/";
        }
    }

    private class GithubRelease
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("assets")]
        public List<GithubReleaseAsset> Assets { get; set; }
    }

    private class GithubReleaseAsset
    {
        [JsonPropertyName("content_type")]
        public string ContentType { get; set; }
        [JsonPropertyName("browser_download_url")]
        public string BrowserDownloadUrl { get; set; }
    }

    private class VersionId : IComparable<VersionId>
    {
        public VersionId(String versionName)
        {
            int lastDash = versionName.LastIndexOf('-');
            var versionBeforeTheLastDash = lastDash == -1 ? versionName : versionName.Substring(0, lastDash);
            var parts = versionBeforeTheLastDash.Split(".");
            var variant = versionBeforeTheLastDash.Length == versionName.Length ? "" : versionName.Substring(versionBeforeTheLastDash.Length + 1);

            int MaybeParseInt(string s)
            {
                return int.TryParse(s, out var result) ? result : 0;
            }
            
            int MaybeParsePart(string[] parts, int index)
            {
                return index >= parts.Length ? 0 : MaybeParseInt(parts[index]);
            }
            
            Major = MaybeParsePart(parts, 0);
            Minor = MaybeParsePart(parts, 1);
            Build = MaybeParsePart(parts, 2);
            VariantType = new string(variant.Where(char.IsLetter).ToArray());
            VariantNumber = MaybeParseInt(new string(variant.Where(char.IsDigit).ToArray()));
        }

        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
        public string VariantType { get; set; }
        public int VariantNumber { get; set; }

        public bool IsStable()
        {
            return VariantType == "";
        }

        public int CompareTo(VersionId? other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var majorComparison = Major.CompareTo(other.Major);
            if (majorComparison != 0) return majorComparison;
            var minorComparison = Minor.CompareTo(other.Minor);
            if (minorComparison != 0) return minorComparison;
            var buildComparison = Build.CompareTo(other.Build);
            if (buildComparison != 0) return buildComparison;
            var variantTypeComparison = string.Compare(VariantType, other.VariantType, StringComparison.Ordinal);
            if (variantTypeComparison != 0) return variantTypeComparison;
            return VariantNumber.CompareTo(other.VariantNumber);
        }
    }
}
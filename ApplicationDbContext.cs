using baam.Model;
using baam.Model.Db;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace baam
{
    public class ApplicationDbContext : DbContext, IDataProtectionKeyContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<AttendanceSession> AttendanceSessions { get; set; } = null!;
        public DbSet<AttendanceMark> AttendanceMarks { get; set; } = null!;
        public DbSet<AutocompleteSelectedSuggestion> AutocompleteSelectedSuggestions { get; set; } = null!;
        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AttendanceSession>();
            
            modelBuilder.Entity<AttendanceMark>()
                .HasIndex(_ => _.Student);

            modelBuilder.Entity<AttendanceMark>()
                .HasOne(_ => _.Session)
                .WithMany(_ => _.Attendance)
                .HasForeignKey(_ => _.SessionId);
            
            modelBuilder.Entity<AttendanceMark>()
                .HasIndex(_ => new {_.Student, _.SessionId, _.Stage})
                .IsUnique();

            modelBuilder.Entity<AttendanceMark>()
                .HasIndex(_ => new {_.Student});

            modelBuilder.Entity<AutocompleteSelectedSuggestion>()
                .HasIndex(_ => new {_.Suggestion, _.User});
            modelBuilder.Entity<AutocompleteSelectedSuggestion>()
                .HasIndex(_ => _.SelectedDate);
        }
    }
}